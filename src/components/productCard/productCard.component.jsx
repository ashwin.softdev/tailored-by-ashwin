import { useDispatch, useSelector } from "react-redux";
import { styled } from "styled-components";
import Button from "../../atoms/common/button";
import { Box } from "../../atoms/common/container";
import { addItemsToCart } from "../../store/cart/cart.action";
import { selectCartItems } from "../../store/cart/cart.selector";

const ProductCard = ({ product }) => {
  const { name, price, imageUrl } = product;
  const cartItems = useSelector(selectCartItems);
  const dispatch = useDispatch();
  const handleAddProductToCart = () => {
    dispatch(addItemsToCart(cartItems, product));
  };

  return (
    <ProductCardContainer>
      <img src={imageUrl} alt={name} />
      <Box className="footer">
        <span className="name">{name}</span>
        <span className="price">{price}</span>
      </Box>
      <Button onClick={handleAddProductToCart}>Add to Cart</Button>
    </ProductCardContainer>
  );
};

const ProductCardContainer = styled(Box)`
  width: 100%;
  display: flex;
  flex-direction: column;
  height: 21.875rem;
  align-items: center;
  position: relative;
  img {
    width: 100%;
    height: 95%;
    object-fit: cover;
    margin-bottom: 0.3125rem;
  }
  button {
    width: 80%;
    opacity: 0.7;
    position: absolute;
    top: 15.9375rem;
    display: none;
  }
  &:hover {
    img {
      opacity: 0.8;
    }
    button {
      opacity: 0.85;
      display: flex;
      justify-content: center;
    }
  }
  .footer {
    width: 100%;
    height: 5%;
    display: flex;
    justify-content: space-between;
    font-size: 1.125rem;
    .name {
      width: 90%;
      margin-bottom: 0.9375rem;
    }
    .price {
      width: 10%;
    }
  }
`;

export default ProductCard;

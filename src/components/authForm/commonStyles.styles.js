import { styled } from "styled-components";
import { Box, FlexBox } from "../../atoms/common/container";

export const SignUpContainer = styled(Box)`
  @media screen and (min-width: 48rem) {
    width: 50%;
    padding-right: 12rem;
  }

  form {
    width: 100%;
  }
`;

export const ButtonContainer = styled(FlexBox)`
  justify-content: ${({ $loginForm }) =>
    $loginForm ? "space-around" : "center"};
  flex-wrap: wrap;
`;

export const SignIn = styled.span`
  text-decoration: underline;
  cursor: pointer;
`;

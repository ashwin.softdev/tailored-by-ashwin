export const SIGN_UP_HEADER = "JOIN US";
export const LOGIN_HEADER = "WELCOME BACK";
export const REGISTER = "Register here";
export const LOGIN = "Sign in";
export const LOGIN_BTN = "LOGIN";
export const SIGN_UP_BTN = "SIGN UP";

export const LOGIN_FORM_FIELDS = [
  {
    label: "Email",
    name: "email",
    placeholder: "Enter your email",
    type: "text",
    required: true,
  },
  {
    label: "Password",
    name: "password",
    placeholder: "Enter your password",
    type: "password",
    required: true,
  },
];

export const SIGN_UP_FORM_FIELDS = [
  {
    label: "Name",
    name: "displayName",
    placeholder: "Enter your name",
    type: "text",
    required: true,
  },
  {
    label: "Email",
    name: "email",
    placeholder: "Enter your email",
    type: "email",
    required: true,
  },
  {
    label: "Password",
    name: "password",
    placeholder: "Enter your password",
    type: "password",
    required: true,
  },
  {
    label: "Confirm Password",
    name: "confirmPassword",
    placeholder: "Confirm your password",
    type: "password",
    required: true,
  },
];

export const initialSignUpFormdata = {
  displayName: "",
  email: "",
  password: "",
  confirmPassword: "",
};

export const initialSignInFormdata = {
  email: "",
  password: "",
};

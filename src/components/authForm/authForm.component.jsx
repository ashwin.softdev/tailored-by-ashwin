import Button from "../../atoms/common/button";
import Input from "../../atoms/common/input";
import {
  LOGIN,
  LOGIN_BTN,
  LOGIN_FORM_FIELDS,
  LOGIN_HEADER,
  REGISTER,
  SIGN_UP_BTN,
  SIGN_UP_FORM_FIELDS,
  SIGN_UP_HEADER,
} from "./authFormConstants";
import {
  ButtonContainer,
  SignIn,
  SignUpContainer,
} from "./commonStyles.styles";

const AuthForm = ({
  handleLoginForm,
  loginForm,
  onSubmit,
  formData,
  setFormData,
  loginUsingGoogle,
  errors,
}) => {
  const handleFormData = (event) => {
    const { name, value } = event.target;
    setFormData({ ...formData, [name]: value });
  };

  return (
    <SignUpContainer>
      <h2>{loginForm ? LOGIN_HEADER : SIGN_UP_HEADER}</h2>
      <form onSubmit={onSubmit}>
        {(loginForm ? LOGIN_FORM_FIELDS : SIGN_UP_FORM_FIELDS).map(
          ({ label, type, placeholder, required, name }) => (
            <Input
              key={label}
              label={label}
              type={type}
              placeholder={placeholder}
              required={required}
              name={name}
              value={formData.name}
              onChange={handleFormData}
              isError={errors[name]}
              error={errors[name]}
            />
          )
        )}
        <p>
          Already registered?{" "}
          <SignIn onClick={handleLoginForm}>
            {loginForm ? REGISTER : LOGIN}
          </SignIn>
        </p>
        <ButtonContainer $loginForm={loginForm}>
          <Button type={"submit"}>{loginForm ? LOGIN_BTN : SIGN_UP_BTN}</Button>
          {loginForm && (
            <Button
              onClick={loginUsingGoogle}
              $buttonType={"thirdPartyBtn"}
              type={"button"}
            >
              LOGIN USING GOOGLE
            </Button>
          )}
        </ButtonContainer>
      </form>
    </SignUpContainer>
  );
};

export default AuthForm;

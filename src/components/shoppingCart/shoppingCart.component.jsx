import { useSelector } from "react-redux";
import { styled } from "styled-components";
import { ReactComponent as ShoppingCartIcon } from "../../assets/shopping-bag.svg";
import { Box } from "../../atoms/common/container";
import { selectCartTotal } from "../../store/cart/cart.selector";

const ShoppingCart = ({ handleCartToggle }) => {
  const totalCartQuantity = useSelector(selectCartTotal);
  return (
    <ShoppingCartContainer onClick={handleCartToggle}>
      <ShoppingCartIcon className="shopping-icon" />
      <span className="item-count">{totalCartQuantity}</span>
    </ShoppingCartContainer>
  );
};

const ShoppingCartContainer = styled(Box)`
  width: 2.8125rem;
  height: 2.8125rem;
  position: relative;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  .shopping-icon {
    width: 1.5rem;
    height: 1.5rem;
  }
  .item-count {
    position: absolute;
    font-size: 0.625rem;
    font-weight: bold;
    bottom: 0.75rem;
  }
`;

export default ShoppingCart;

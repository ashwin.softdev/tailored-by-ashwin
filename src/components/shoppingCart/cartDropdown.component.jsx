import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { styled } from "styled-components";
import Button from "../../atoms/common/button";
import { Box } from "../../atoms/common/container";
import { setIsCartOpen } from "../../store/cart/cart.action";
import { selectCartItems } from "../../store/cart/cart.selector";
import CartItem from "./cartItem.component";

const CartDropdown = () => {
  const dispatch = useDispatch();
  const cartItems = useSelector(selectCartItems);
  const navigate = useNavigate();
  const handleGotoCheckout = () => {
    navigate("checkout");
    dispatch(setIsCartOpen());
  };
  return (
    <CardDropdownContainer>
      {Array.isArray(cartItems) && cartItems.length === 0 ? (
        <EmptyCartText>Cart is empty</EmptyCartText>
      ) : (
        <>
          <Box className="cart-items">
            {cartItems.map((item) => (
              <CartItem key={item.id} cartItem={item} />
            ))}
          </Box>
          <Button onClick={handleGotoCheckout}>GO TO CHECKOUT</Button>
        </>
      )}
    </CardDropdownContainer>
  );
};

const CardDropdownContainer = styled(Box)`
  position: absolute;
  width: 15rem;
  height: 21.25rem;
  display: flex;
  flex-direction: column;
  padding: 1.25rem;
  border: 0.0625rem solid black;
  background-color: white;
  top: 5rem;
  right: 1.5rem;
  z-index: 5;
  .empty-message {
    font-size: 1.125rem;
    margin: 3.125rem auto;
  }
  .cart-items {
    height: 15rem;
    display: flex;
    flex-direction: column;
    overflow: scroll;
  }
  button {
    margin-top: auto;
  }
`;

const EmptyCartText = styled.p`
  text-align: center;
`;

export default CartDropdown;

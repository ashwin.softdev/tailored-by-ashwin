import { styled } from "styled-components";
import { Box } from "../../atoms/common/container";

const CartItem = ({ cartItem }) => {
  const { name, quantity, imageUrl, price } = cartItem;
  return (
    <CartItemContainer>
      <img src={imageUrl} alt={name} />
      <Box className="item-details">
        <span className="name">{name}</span>
        <span className="price">
          {quantity} x ${price}
        </span>
      </Box>
    </CartItemContainer>
  );
};

const CartItemContainer = styled(Box)`
  width: 100%;
  display: flex;
  height: 5rem;
  margin-bottom: 0.9375rem;
  img {
    width: 30%;
  }
  .item-details {
    width: 70%;
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;
    padding: 0.625rem 1.25rem;
    .name {
      font-size: 1rem;
    }
  }
`;

export default CartItem;

import { useDispatch, useSelector } from "react-redux";
import { styled } from "styled-components";
import { Box } from "../../atoms/common/container";
import {
  addItemsToCart,
  reduceItemQuantity,
  removeItemsFromCart,
} from "../../store/cart/cart.action";
import { selectCartItems } from "../../store/cart/cart.selector";

const CheckoutItem = ({ cartItem }) => {
  const { name, imageUrl, quantity, price } = cartItem;
  const cartItems = useSelector(selectCartItems);
  const dispatch = useDispatch();
  const handleIncreaseQuantity = () =>
    dispatch(addItemsToCart(cartItems, cartItem));
  const handleReduceQuantity = () =>
    dispatch(reduceItemQuantity(cartItems, cartItem));
  const handleRemoveItems = () =>
    dispatch(removeItemsFromCart(cartItems, cartItem));

  return (
    <CheckoutItemContainer>
      <Box className="image-container">
        <img src={imageUrl} alt={name} />
      </Box>
      <span className="name">{name}</span>
      <span className="quantity">
        <div className="arrow" onClick={handleReduceQuantity}>
          &#10094;
        </div>
        <span className="value">{quantity}</span>
        <div className="arrow" onClick={handleIncreaseQuantity}>
          &#10095;
        </div>
      </span>
      <span className="price">{price}</span>
      <span className="remove-item" onClick={handleRemoveItems}>
        &#10005;
      </span>
    </CheckoutItemContainer>
  );
};

const CheckoutItemContainer = styled(Box)`
  width: 100%;
  display: flex;
  min-height: 100px;
  border-bottom: 0.0625rem solid darkgrey;
  padding: 0.9375rem 0;
  font-size: 1.25rem;
  align-items: center;
  .image-container {
    width: 23%;
    padding-right: 0.9375rem;
    img {
      width: 100%;
      height: 100%;
    }
  }
  .name,
  .quantity,
  .price {
    width: 23%;
  }
  .quantity {
    display: flex;
    .arrow {
      cursor: pointer;
    }
    .value {
      margin: 0 0.625rem;
    }
  }
  .remove-item {
    padding-left: 0.75rem;
    cursor: pointer;
  }
`;

export default CheckoutItem;

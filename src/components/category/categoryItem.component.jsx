import { useNavigate } from "react-router-dom";
import { Box } from "../../atoms/common/container";

const CategoryItem = ({ category }) => {
  const navigate = useNavigate();
  const { title, imageUrl } = category;
  const handleProductRedirect = () => {
    navigate(`shop/${title}`);
  };
  return (
    <Box className="category-container">
      <Box
        className="background-image"
        style={{ backgroundImage: `url(${imageUrl})` }}
      />
      <Box onClick={handleProductRedirect} className="category-body-container">
        <h2>{title}</h2>
        <p>Shop Now</p>
      </Box>
    </Box>
  );
};

export default CategoryItem;

import { useNavigate } from "react-router-dom";
import { styled } from "styled-components";
import { Box } from "../../atoms/common/container";
import ProductCard from "../productCard/productCard.component";

const CategoryPreview = ({ title, products }) => {
  const navigate = useNavigate();
  const handleProductRedirect = () => {
    navigate(`${title}`);
  };
  return (
    <CategoryPreviewContainer>
      <h2 onClick={handleProductRedirect}>
        <span className="title">{title.toUpperCase()}</span>
      </h2>
      <Box className="preview">
        {(products || [])
          .filter((_, index) => index < 4)
          .map((product) => (
            <ProductCard key={product.id} product={product} />
          ))}
      </Box>
    </CategoryPreviewContainer>
  );
};

const CategoryPreviewContainer = styled(Box)`
  display: flex;
  flex-direction: column;
  margin-bottom: 1.875rem;
  .title {
    font-size: 1.75rem;
    margin-bottom: 1.5625rem;
    cursor: pointer;
  }
  .preview {
    display: grid;
    grid-template-columns: repeat(4, 1fr);
    column-gap: 1.25rem;
  }
`;

export default CategoryPreview;

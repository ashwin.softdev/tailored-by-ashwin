import { forwardRef, useRef } from "react";
import { styled } from "styled-components";
import { Box } from "./container";

const Input = forwardRef((props, ref) => {
  const {
    value,
    onChange,
    required,
    type,
    placeholder,
    label,
    isError = false,
    error,
    name,
  } = props;
  const inputRef = useRef(ref);
  const handleContainerClick = () => {
    inputRef.current.focus();
  };
  return (
    <OuterContainer>
      <InputContainer onClick={handleContainerClick}>
        <label>{label}</label>
        <input
          type={type}
          ref={inputRef}
          value={value}
          onChange={onChange}
          required={required}
          placeholder={placeholder}
          name={name}
        />
      </InputContainer>
      {isError && <Error>{error}</Error>}
    </OuterContainer>
  );
});

const InputContainer = styled(Box)`
  border: 0.0625rem solid black;
  border-radius: 0.375rem;
  width: auto;
  padding: 0.45rem;
  cursor: text;
  label {
    font-size: 0.875rem;
    display: inline-block;
    opacity: 0.4;
  }
  input {
    all: unset;
    width: 100%;
  }
`;

const OuterContainer = styled(Box)`
  margin-bottom: 2rem;
`;

const Error = styled.span`
  margin-top: 0.2rem;
  display: block;
  font-size: 0.7rem;
  font-weight: 400;
  color: red;
`;

export default Input;

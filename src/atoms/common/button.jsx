import { forwardRef } from "react";
import { styled } from "styled-components";

const Button = forwardRef((props, ref) => {
  const { children, type, $buttonType, onClick } = props;
  return (
    <StyledButton
      onClick={onClick}
      ref={ref}
      type={type}
      $buttonType={$buttonType}
    >
      {children}
    </StyledButton>
  );
});

export default Button;

const StyledButton = styled.button`
  all: unset;
  min-width: 8rem;
  border-radius: 0.5rem;
  padding: 0.75rem;
  text-align: center;
  color: white;
  border: 0.0625rem solid black;
  cursor: pointer;
  background-color: ${({ $buttonType }) =>
    $buttonType ? "#4285F4" : "black"}};
`;

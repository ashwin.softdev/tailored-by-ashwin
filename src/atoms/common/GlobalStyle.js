import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    body {
        margin: 0;
        font-family: "Open Sans";
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        // background-color: #404258;
      }
      
      code {
        font-family: "Open Sans";
      }
      
      a {
        all: unset;
        cursor: pointer;
        color: black;
      }
      *{
        box-sizing: border-box;
      }
`;

export default GlobalStyle;

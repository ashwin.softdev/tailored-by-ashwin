import { createAction } from "../../utils/reducer/reducer.utils";
import { CATEGORY_ACTION_TYPES } from "./category.types";

export const setProductCategory = (categoriesArray) =>
  createAction(CATEGORY_ACTION_TYPES.SET_PRODUCT_CATEGORY, categoriesArray);

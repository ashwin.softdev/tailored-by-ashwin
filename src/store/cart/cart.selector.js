import { createSelector } from "reselect";

const cartReducer = (state) => state.cart;

export const selectIsCartOpen = createSelector(
  [cartReducer],
  (cart) => cart.isCartOpen
);

export const selectCartItems = createSelector(
  [cartReducer],
  (cart) => cart.cartItems
);

export const selectCartTotal = createSelector([selectCartItems], (cartItems) =>
  cartItems.reduce(
    (prev, current) => prev + Number(current.quantity) * Number(current.price),
    0
  )
);

export const selectCartCount = createSelector([selectCartItems], (cartItems) =>
  cartItems.reduce((total, cartItem) => total + cartItem.quantity, 0)
);

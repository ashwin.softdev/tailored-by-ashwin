import { createAction } from "../../utils/reducer/reducer.utils";
import { SHOPPING_CART_ACTION_TYPES } from "./cart.types";

export const setIsCartOpen = () =>
  createAction(SHOPPING_CART_ACTION_TYPES.IS_CART_OPEN);

const addCartItem = (addedProduct, cartItems) => {
  const existingCartItem = cartItems.find(
    (cartItem) => cartItem.id === addedProduct.id
  );
  if (existingCartItem) {
    return cartItems.map((cartItem) =>
      cartItem.id === addedProduct.id
        ? { ...cartItem, quantity: cartItem.quantity + 1 }
        : cartItem
    );
  }
  return [...cartItems, { ...addedProduct, quantity: 1 }];
};

export const removeItemsFromCart = (cartItems, productToRemove) => {
  const updatedCartItems = cartItems.filter(
    (item) => item.id !== productToRemove.id
  );
  return createAction(
    SHOPPING_CART_ACTION_TYPES.SET_CART_ITEMS,
    updatedCartItems
  );
};

export const reduceItemQuantity = (cartItems, itemToRemove) => {
  if (itemToRemove.quantity <= 1) {
    return removeItemsFromCart(itemToRemove);
  }
  const updatedCartItems = cartItems.map((item) =>
    item.id === itemToRemove.id
      ? { ...item, quantity: item.quantity - 1 }
      : item
  );
  return createAction(
    SHOPPING_CART_ACTION_TYPES.SET_CART_ITEMS,
    updatedCartItems
  );
};

export const addItemsToCart = (cartItems, addedProduct) => {
  return createAction(
    SHOPPING_CART_ACTION_TYPES.SET_CART_ITEMS,
    addCartItem(addedProduct, cartItems)
  );
};

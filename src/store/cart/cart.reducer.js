import { SHOPPING_CART_ACTION_TYPES } from "./cart.types";

const SHOPPING_CART_INITIAL_STATE = {
  isCartOpen: false,
  cartItems: [],
};

export const shoppingCartReducer = (
  state = SHOPPING_CART_INITIAL_STATE,
  action = {}
) => {
  const { type, payload } = action;
  switch (type) {
    case SHOPPING_CART_ACTION_TYPES.SET_CART_ITEMS:
      return {
        ...state,
        cartItems: payload,
      };

    case SHOPPING_CART_ACTION_TYPES.IS_CART_OPEN:
      return {
        ...state,
        isCartOpen: !state.isCartOpen,
      };

    default:
      return state;
  }
};

import { combineReducers } from "redux";
import { shoppingCartReducer } from "./cart/cart.reducer";
import { categoriesReducer } from "./category/category.reducer";
import { userReducer } from "./user/user.reducer";

export const rootReducer = combineReducers({
  user: userReducer,
  categories: categoriesReducer,
  cart: shoppingCartReducer,
});

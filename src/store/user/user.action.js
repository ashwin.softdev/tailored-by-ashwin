import { createAction } from "../../utils/reducer/reducer.utils";
import { USER_ACTION_TYPES } from "./user.types";

export const setAuthUser = (user) =>
  createAction(USER_ACTION_TYPES.SET_AUTH_USER, user);

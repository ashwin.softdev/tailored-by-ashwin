import { USER_ACTION_TYPES } from "./user.types";

const INITIAL_STATE = {
  authUser: null,
};

export const userReducer = (state = INITIAL_STATE, action) => {
  const { type, payload } = action;
  switch (type) {
    case USER_ACTION_TYPES.SET_AUTH_USER:
      return {
        ...state,
        authUser: payload,
      };
    default:
      return state;
  }
};

import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { Route, Routes } from "react-router-dom";
import Auth from "./layouts/auth/auth.layout";
import Checkout from "./layouts/checkout/checkout.layout";
import Home from "./layouts/home/home.layout";
import Navigation from "./layouts/navigation/navigation.layout";
import Shop from "./layouts/shop/shop.layout";
import { setAuthUser } from "./store/user/user.action";
import {
  createDocAuthFromAuth,
  onAuthStateChangeListener,
} from "./utils/firebase/firebase.utils";

const App = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const unsubscribe = onAuthStateChangeListener((user) => {
      if (user) {
        createDocAuthFromAuth(user);
      }
      dispatch(setAuthUser(user));
    });
    return unsubscribe;
  }, []);

  return (
    <Routes>
      <Route path="/" element={<Navigation />}>
        <Route index element={<Home />} />
        <Route path="shop/*" element={<Shop />} />
        <Route path="auth" element={<Auth />} />
        <Route path="checkout" element={<Checkout />} />
      </Route>
    </Routes>
  );
};

export default App;

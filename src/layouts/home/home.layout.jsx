import CategoryContainer from "../../components/category/categoryContainer.component";
import { categories } from "./homeConstants";

const Home = () => {
  return (
    <>
      <CategoryContainer categories={categories} />;
    </>
  );
};

export default Home;

import { useSelector } from "react-redux";
import CategoryPreview from "../../components/categoryPreview/categoryPreview.component";
import { selectCategories } from "../../store/category/category.selector";

const CategoriesPreview = () => {
  const categories = useSelector(selectCategories);
  return (
    <>
      {Object.keys(categories).map((title, idx) => (
        <CategoryPreview key={idx} title={title} products={categories[title]} />
      ))}
    </>
  );
};

export default CategoriesPreview;

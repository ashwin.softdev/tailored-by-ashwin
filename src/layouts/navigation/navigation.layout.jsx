import { useDispatch, useSelector } from "react-redux";
import { Link, Outlet } from "react-router-dom";
import { styled } from "styled-components";
import logo from "../../assets/logo.png";
import { Box, FlexBox } from "../../atoms/common/container";
import CartDropdown from "../../components/shoppingCart/cartDropdown.component";
import ShoppingCart from "../../components/shoppingCart/shoppingCart.component";
import { setIsCartOpen } from "../../store/cart/cart.action";
import { selectIsCartOpen } from "../../store/cart/cart.selector";
import { selectAuthUser } from "../../store/user/user.selector";
import { signOutAuthUser } from "../../utils/firebase/firebase.utils";

const Navigation = () => {
  const authUser = useSelector(selectAuthUser);
  const dispatch = useDispatch();
  const isCartOpen = useSelector(selectIsCartOpen);

  const handleSignOut = async () => {
    await signOutAuthUser();
  };

  const handleCartToggle = () => {
    dispatch(setIsCartOpen());
  };

  return (
    <>
      <NavigationContainer>
        <Box>
          <Link to="/">
            <Logo src={logo} alt="shop logo" />
          </Link>
        </Box>
        <LinksContainer>
          <Link to="/shop">SHOP</Link>
          <Link to="/auth" onClick={handleSignOut}>
            {authUser ? "SIGN OUT" : "SIGN IN"}
          </Link>
          <ShoppingCart handleCartToggle={handleCartToggle} />
        </LinksContainer>
        {isCartOpen && <CartDropdown />}
      </NavigationContainer>
      <OutletContainer>
        <Outlet />
      </OutletContainer>
    </>
  );
};

const NavigationContainer = styled(FlexBox)`
  background-color: white;
  align-items: center;
  justify-content: space-between;
  padding: 1.25rem;
  margin-bottom: 0.8rem;
  box-shadow: 1px 1px 4px 1px rgba(211, 211, 211, 0.5);
  position: fixed;
  top: 0;
  width: 100%;
  z-index: 999;
`;

const LinksContainer = styled(FlexBox)`
  gap: 0.625rem;
  align-items: center;
`;

const Logo = styled.img`
  height: 50px;
  width: 150px;
`;

const OutletContainer = styled(Box)`
  margin-top: 7.5rem;
  padding: 0 2.5rem;
  margin-bottom: 3rem;
`;

export default Navigation;

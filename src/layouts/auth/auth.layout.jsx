import { useState } from "react";
import { styled } from "styled-components";
import { FlexBox } from "../../atoms/common/container";
import AuthForm from "../../components/authForm/authForm.component";
import {
  initialSignInFormdata,
  initialSignUpFormdata,
} from "../../components/authForm/authFormConstants";
import {
  createDocAuthFromAuth,
  signInAuthWithEmailAndPassword,
  signInWithGooglePopup,
  signUpAuthWithEmailandPassword,
} from "../../utils/firebase/firebase.utils";

const Auth = () => {
  const [loginForm, setLoginForm] = useState(true);
  const initializeFormData = loginForm
    ? initialSignInFormdata
    : initialSignUpFormdata;
  const [formData, setFormData] = useState(initializeFormData);
  const [errors, setErrors] = useState({});

  const handleSubmit = async (event) => {
    event.preventDefault();
    const { email, password } = formData;
    if (loginForm) {
      try {
        await signInAuthWithEmailAndPassword(email, password);
      } catch (error) {
        switch (error.code) {
          case "auth/wrong-password":
            setErrors({ password: "Email or password is wrong" });
            break;
          case "auth/user-not-found":
            setErrors({ email: "User does not exist" });
            break;
          default:
            alert("Something went wrong");
        }
      }
    } else {
      if (password !== formData?.confirmPassword) {
        setErrors({ password: `Passwords doesn't match` });
        return;
      }
      try {
        const { user } = await signUpAuthWithEmailandPassword(email, password);
        await createDocAuthFromAuth(user, {
          displayName: formData?.displayName,
        });
        event.target.reset();
      } catch (error) {
        if (error.code === "auth/email-already-in-use") {
          alert("Email aready in use. Please sign in.");
        } else {
          alert("Something went wrong");
        }
      }
    }
  };

  const handleLoginForm = () => {
    setLoginForm(!loginForm);
    setFormData(initializeFormData);
  };

  const loginUsingGoogle = async () => {
    await signInWithGooglePopup();
  };

  return (
    <AuthContainer>
      <AuthForm
        onSubmit={handleSubmit}
        formData={formData}
        setFormData={setFormData}
        loginForm={loginForm}
        handleLoginForm={handleLoginForm}
        loginUsingGoogle={loginUsingGoogle}
        errors={errors}
      />
    </AuthContainer>
  );
};

const AuthContainer = styled(FlexBox)`
  justify-content: center;
`;

export default Auth;

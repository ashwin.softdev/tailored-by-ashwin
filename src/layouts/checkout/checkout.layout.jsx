import { useSelector } from "react-redux";
import { styled } from "styled-components";
import { Box } from "../../atoms/common/container";
import CheckoutItem from "../../components/checkout/checkoutItem.component";
import {
  selectCartItems,
  selectCartTotal,
} from "../../store/cart/cart.selector";

const CHECKOUT_HEADERS = [
  "Product",
  "Description",
  "Quantity",
  "Price",
  "Remove",
];

const Checkout = () => {
  const totalAmount = useSelector(selectCartTotal);
  const cartItems = useSelector(selectCartItems);
  return (
    <>
      <CheckoutContainer>
        <Box className="checkout-header">
          {CHECKOUT_HEADERS.map((header, index) => (
            <Box key={index} className="header-block">
              {header}
            </Box>
          ))}
        </Box>
        {cartItems.map((cartItem) => (
          <CheckoutItem key={cartItem.id} cartItem={cartItem} />
        ))}
        <span className="total">Total : ${totalAmount}</span>
      </CheckoutContainer>
    </>
  );
};

const CheckoutContainer = styled(Box)`
  width: 90%;
  min-height: 90vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 3.125rem auto 0;
  .checkout-header {
    width: 100%;
    padding: 0.625rem 0;
    display: flex;
    justify-content: space-between;
    border-bottom: 0.0625rem solid darkgrey;
    .header-block {
      text-transform: capitalize;
      width: 23%;
      &:last-child {
        width: 8%;
      }
    }
  }
  .total {
    margin-top: 1.875rem;
    margin-left: auto;
    font-size: 2.25rem;
  }
`;

export default Checkout;

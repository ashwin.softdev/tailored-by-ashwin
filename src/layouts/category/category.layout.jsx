import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { styled } from "styled-components";
import { Box } from "../../atoms/common/container";
import ProductCard from "../../components/productCard/productCard.component";
import { selectCategories } from "../../store/category/category.selector";

const Category = () => {
  const categories = useSelector(selectCategories);
  const { category } = useParams();
  const [products, setProducts] = useState(categories[category]);
  useEffect(() => {
    setProducts(categories[category]);
  }, [categories, category]);
  return (
    <>
      <CategoryTitle>{category.toUpperCase()}</CategoryTitle>
      <CategoryContainer>
        {(products || []).map((product) => (
          <ProductCard key={product.id} product={product} />
        ))}
      </CategoryContainer>
    </>
  );
};

const CategoryContainer = styled(Box)`
  display: grid;
  grid-template-columns: repeat(4, 1fr);
  column-gap: 1.25rem;
  row-gap: 3.125rem;
`;

const CategoryTitle = styled.h1`
  text-align: center;
  margin-bottom: 1.5rem;
`;

export default Category;
